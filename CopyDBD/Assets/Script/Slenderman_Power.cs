using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slenderman_Power : MonoBehaviour
{
    [SerializeField] private GameObject obj;
    public GameObject player;
    [SerializeField] private float radius;
    [SerializeField] private GameObject slenderman;
    [SerializeField] private AudioSource audiosource;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButtonDown("Power")) 
        if (Input.GetKeyDown(KeyCode.E))
        {
           // print("Inutil");
            Teleport();
        }
        
    }

    public void Teleport()
    {
  

        float posicionEnX = Random.Range(-radius,radius);
        float posicionEnZ = Mathf.Sign(Random.Range(-1,1))*Mathf.Sqrt(Mathf.Pow(radius,2)- Mathf.Pow(posicionEnX, 2));
  

        Vector3 futurePosition = new Vector3(posicionEnX, 0, posicionEnZ)+player.transform.position;

        GameObject newCilindro = Instantiate(obj, futurePosition,Quaternion.identity);
        
        slenderman.transform.position = newCilindro.transform.position;
        audiosource.Play();
        Destroy(newCilindro);
    }


}
