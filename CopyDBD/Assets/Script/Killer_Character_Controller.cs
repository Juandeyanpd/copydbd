using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killer_Character_Controller : MonoBehaviour
{
    #region Variables
    public CharacterController controller;
    public Animator killer_control;
    
    [Header("Movimiento del personaje")]
    public float speedMovement;
    public Vector3 direccion;

    [Header("Movimiento de la c�mara")]
    public Vector2 mouseMovement;
    public Camera playerCamera;
    public float rotacionCamaraX;
    public float rotacionPlayerY;

    #endregion

    #region Funciones de unity
    private void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        Mover(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));

        Rotarpersonaje(Input.GetAxis("Mouse X"));

        Rotarcamara(Input.GetAxis("Mouse Y"));

        Atack();                    
    }
    #endregion

    #region Funciones custom
    public void Mover(float vertical, float horizontal)
    {
        direccion.x = horizontal;
        direccion.z = vertical;

        direccion = transform.TransformDirection(direccion);

        controller.Move(direccion * Time.deltaTime * speedMovement);
        killer_control.SetFloat("Speed", vertical);
    }

    public void Rotarpersonaje(float rotacionpersonaje)
    {

        rotacionPlayerY += rotacionpersonaje;

        controller.transform.rotation = Quaternion.Euler(0, rotacionPlayerY, 0);

    }

    public void Rotarcamara(float rotacioncamara)
    {

        rotacionCamaraX += rotacioncamara;
        rotacionCamaraX = Mathf.Clamp(rotacionCamaraX, -60, 90);
        playerCamera.transform.localRotation = Quaternion.Euler(-rotacionCamaraX, 0, 0);

    }

    public void Atack()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            killer_control.SetTrigger("Atack");
        }
    }

    #endregion

}



